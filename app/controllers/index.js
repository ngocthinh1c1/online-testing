// lấy data từ DB
// arrow function
// khác  biệt duy nhất arrow không làm thay đổi ngữ cảnh con trỏ this
// trước khi vào hàm this trỏ tới gì thì khi vào hàm vẫn giữu nguyên trỏ this
// mặc định con trỏ this trỏ tới đối tượng window

const questionList = [];
import FillInBlank from '../../models/fillInBlank.js'
import MultipleChoice from '../../models/multipleChoice.js'

const getData = () => {
  axios({
    method: "GET",
    url: "../../DeThiTracNghiem.json"
  })
    .then(res => {
      mapDataToObject(res.data);

      // map() cùng  1 số lượng phần tử
      var contentarr = questionList.map( (currentQuestion, index) => {
        return currentQuestion.content;
      } );
      console.log(contentarr);
      


      renderQuestion();
    })
    .catch(err => {
      console.log(err);
    });
  // trả về đối tượng promise, dữ liệu gửi lên là bất đồng bộ nên ta ko b đc co
  // trả về hay không, thậm chí không trả
  // promise có 3 trạng thái: pending, resolve, reject
};

const mapDataToObject = data => {
  for (let question of data) {
    // destructuring : bóc tách phần tử
    const { questionType, _id, content, answers } = question;
    // => const questionType = question.questionType;
    let newQuestion = {};
    if (questionType === 1) {
      newQuestion = new MultipleChoice(questionType, _id, content, answers);
    } else {
      newQuestion = new FillInBlank(questionType, _id, content, answers);
    }
    questionList.push(newQuestion);
  }
};

const renderQuestion = () =>{
    let content =  '';
    for(let i in questionList){
        content += questionList[i].render(i*1+1);
    }
    document.getElementById("content").innerHTML = content;
}

getData();

document.getElementById("btnSubmit").addEventListener("click", ()=>{
  let result = 0;
  // forr or trả ra nguyên đối tượng
  // forr in trả về index
  // for in duyệt đc object
  // for(let question of questionList){
  //   // có check exact thì mới chạy
  //    result += question.checkExact(); 
  // }
  result = questionList.reduce((sum,currentQuestion) => {
    return sum += currentQuestion.checkExact(); // phải có return
  }, 0); // tham số khởi tạo sum = 0;
  console.log(result);
})
document.getElementById("btnFilterByType1").addEventListener("click", () => {
    var multipleChoices = questionList.filter((currentQuestion,index) => {
       return currentQuestion.questionType === 1;
  });
  console.log(multipleChoices);
  
})

 /**
  * 
  * map()
  * 
  * filter()
  * 
  * reduce()
  */