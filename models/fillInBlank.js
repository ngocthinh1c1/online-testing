import Question from './question.js'
class FillInBlank extends Question{

    // default value, rest parameters có 2 loại: gộm tách ...args là 1 mảng
    constructor(...args){
        // spread operator
        console.log(...args);
        super(...args);
    }

    render(index){
        // string template
        return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content}</h4>
                <input type = "text" id="${this._id}" />
            </div>
        `
    }
    checkExact() {
        if(document.getElementById(this._id).value.toLowerCase() === this.answers[0].content.toLowerCase()){
            return true;
        }
        return false;
    }
}

export default FillInBlank;
/**
 * 
 * export định danh
 * cái được export ra từ file question là 1 object { Question: Question  CalcSum : CalcSum }
 * Dùng rất nhiều trong angular và react : 
 * 1 Component thực chất là class, 1 file có đúng 1 component
 * 
 * export default
 * 
 * export default Question;
 * import abc from "" (abc có thể dùng được no là cái mà mình export ra)
 * 
 * export default {
 *      a: Question,
 *      b: calcSUm
 * }
 * import obj from
 * obj.a();
 * 
 * 
 * phải thêm type module
 * 
 * 
 * 
 */
