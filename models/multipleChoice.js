import Question from "./question.js";

class MultipleChoice extends Question {
  // rest parameters
  constructor(...args) {
    console.log(...args);
    super(...args);
  }
  renderAnswer() {
    // var let tồn tại song song, let khắc phục những cái hạn chế  của var
    // let không cho khai báo biến trong cùng 1 khu vực var cho phép
    // phạm vi sử dụng : var trong nguyên 1 function let trong 1 cặp ngoặt nhọn
    /**
     * {
     *      let a = 5;
     *      var b = 10;
     * }
     * console.log(a,b);
     */
    /**
     * for(var i =0; i < 5; i++){
     *      setTimeOut(function(){
     *          console.log(i);
     *      }, 500);
     * }
     * 55555
     */
    let answerText = "";
    for (let ans of this.answers) {
      answerText += `
                <div>
                    <input type = "radio" class="question-${this._id}" value="${
        ans._id
      }" name = "${this._id}" />
                    <span>${ans.content}</span>
                </div>
            `;
    }
    return answerText;
  }
  render(index) {
    // string template
    return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content}</h4>
                ${this.renderAnswer()}
            </div>
        `;
  }
  checkExact() {
    // document.getElementsByClassName('question-' + this._id).forEach((answer)=> {
    //     console.log(answer);

    // });
    // spread operator
    let exact = false;
    [...document.getElementsByClassName("question-" + this._id)].forEach(
      ans => {
        if (ans.checked) {
            const ansID = ans.value;
            // this.answers.forEach(element => {
            //     if(element._id === ansID){
            //         console.log(element.exact);  
            //     }
            // });
            // forEach chỉ duyệt đc mảng
            // 3 dấu = so sánh cả kiểu dữ liệu
            // for  in cho duyệt object

            const index = this.answers.find( (ans) => {return ans._id === ansID.toString()}) || {};
            exact = index.exact || false;

            // for(let i in this.answers){
            //     if(this.answers[i]._id === ansID.toString())
            //     {
            //         console.log(this.answers[i].exact); 
            //     }
            // }
        }
      }
    );
    return exact;
  }
}
export default MultipleChoice;

/**
 * var a = { hoten : "hieu"};
 * var b  = { ...a, tuoi : 12 };
 */

 /**
  * 
  * map()
  * 
  * filter()
  * 
  * reduce()
  */
